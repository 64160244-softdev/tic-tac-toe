/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.tictactoe;

import java.util.Scanner;

/**
 *
 * @author thanc
 */
public class TicTacToe {
    private char[][] board;
    
    public void newBoard(){
        board = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }
    
    private void printBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }
    
    public boolean hasWon(char player){
        //check row
        for(int i=0; i<3; i++){
            if(board[i][0] == player && board[i][1]== player&& board[i][2]== player){
                return true;
            }
        }
        
        //check col
        for(int i=0; i<3; i++){
            if(board[0][i]== player&&board[1][i]== player && board[2][i]== player ){
                return true;
            }
        }
        
        //check diagonals
        if(board[0][0]== player&&board[1][1]== player && board[2][2]== player){
            return true;     
        }
        else if(board[0][2]== player&&board[1][1]== player && board[2][0]== player){
            return true;  
        }
        return false;
    }
    
    public boolean isBoardFull(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if(board[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
    }
    
    public void play() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Player 1, choose 'X' or 'O': ");
        char player1 = sc.nextLine().toUpperCase().charAt(0);

        while (player1 != 'X' && player1 != 'O') {
            System.out.print("Invalid choice. Please choose 'X' or 'O': ");
            player1 = sc.nextLine().toUpperCase().charAt(0);
        }

        char player2 = (player1 == 'X') ? 'O' : 'X';

        System.out.println("Player 1: " + player1);
        System.out.println("Player 2: " + player2);
        newBoard();
        printBoard();
        char currentPlayer = player1;
        boolean gameEnded = false;
        while(!gameEnded){
            System.out.println("It's player " + currentPlayer + "'s turn.");
            System.out.print("Please input row [1-3] and column [1-3] for your move [e.g., 1 1] : ");
            int row = sc.nextInt() -  1;
            int col = sc.nextInt() - 1;
           
            if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-') {
                board[row][col] = currentPlayer;
                printBoard();
                if(hasWon(currentPlayer)){
                    System.out.println("Congratulations! Player "+currentPlayer+" wins!");
                    gameEnded = true;
                }else if(isBoardFull()){
                    System.out.println("Draw!");
                    gameEnded = true;
                }else{
                    currentPlayer = (currentPlayer == player1) ? player2 : player1;     
                }
            }else{
                System.out.println("Invalid move Please try agian.");
            }
        }
        System.out.print("Do you want to play again? (y/n) : ");
        String newGame = sc.next();
        if(newGame.equalsIgnoreCase("y")){
            play();
        }else{
           System.out.println("Thank you for playing! Goodbye!");
        }      
    }
    
    public static void main(String[] args) {
        System.out.println("|--------------------|");
        System.out.println("| Welcome To OX Game |");
        System.out.println("|--------------------|");
        TicTacToe game = new TicTacToe();
        game.play();
    }
}
